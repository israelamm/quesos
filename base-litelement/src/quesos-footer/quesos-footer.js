import { LitElement, html, css } from 'lit-element';

class QuesosFooter extends LitElement {

    static get properties() {
        return {
        };
    }

    static get styles(){
        return css`
              div {
                  overflow:hidden;
                  position:fixed;
                  bottom:0px;
                  background: #888;
                  color:#ccc;
                  width:100%;
                  height:50px;
                  text-align:center;
              }
        `
    }

    constructor() {
        super();
    }

    render() {
        return html`
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
        <div align="center" padding="">
            <h5>@Quesos App 2020</h5>
        </div>
        `;
    }
}

customElements.define('quesos-footer', QuesosFooter)