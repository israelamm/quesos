import { LitElement, html } from 'lit-element';  

class QuesoForm extends LitElement {
	static get properties() {
		return {		
            queso:{}
		};
	}

	constructor() {

        super();

        this.queso = this.queso || {};
                this.queso.name = this.queso.name || "";
                this.queso.name = this.queso.description || "";
                this.queso.image = {
                                "src": "./img/Caerphilly_cheese.jpg",
                                "height": 100,
                                "width": 50
                        };
	}

	render() {
		return html`	
            <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
			<div>
				<form>
					<div class="form-group">
						<label>Nombre del Queso</label>
						<input type="text" .value="${this.evaluar(this.queso.name)}" @input="${this.updateName}" id="quesoFormName" class="form-control" placeholder="Nombre"/>
					<div>
					<div class="form-group">
						<label>Descripcion</label>
						<textarea .value="${this.evaluar(this.queso.description)}" @input="${this.updateDescription}" class="form-control" placeholder="Descripcion" rows="5"></textarea>
					<div>
					<div class="form-group">
						<label>Edad</label>
						<input .value="${this.evaluar(this.queso.age)}" @input="${this.updateAge}" type="text" class="form-control" placeholder="Edad"/>
					<div>
					<button @click="${this.goBack}" class="btn btn-primary"><strong>Atrás</strong></button>
					<button @click="${this.storeQueso}" class="btn btn-success"><strong>Guardar</strong></button>
				</form>
			</div>
		`;
	}    
    goBack(e) {
        console.log("goBack");	  
        e.preventDefault();	
        this.dispatchEvent(new CustomEvent("quesos-form-close",{}));	
    }

    updateName(e) {
        console.log("updateName");
        console.log("Actualizando la propiedad name con el valor " + e.target.value);
        this.queso.name = e.target.value;
    }
    updateDescription(e) {
        console.log("updateDescription");
        console.log("Actualizando la propiedad description con el valor " + e.target.value);
        this.queso.description = e.target.value;
    }
    updateAge(e) {
        console.log("updateAge");
        console.log("Actualizando la propiedad Age con el valor " + e.target.value);
        this.queso.age = e.target.value;
    }

    evaluar(valor){
        console.log("en evaluar: "+valor)
        return valor===undefined?"":valor;
    }
    

    storeQueso (e){
        console.log("storeQueso");
        e.preventDefault();

        this.queso.image = {
            "src" : "./img/Caerphilly_cheese.jpg",
            "height" : "100",
            "width" : "100"       
        }

        console.log("la propiedad name vale "+this.queso.name);
        console.log("la propuedad description vale "+this.queso.description);
        console.log("la propiedad Age vale "+this.queso.age);

        this.dispatchEvent( 
            new CustomEvent(
                'quesos-form-store',
                {
                    detail:{
                        queso:{
                            name: this.queso.name,
                            description:this.queso.description,
                            age:this.queso.age,
                            image: this.queso.image
                        }
                    }
                }
            )
        );

        this.queso = {};
        console.log("se borro el queso")
    }

    clearData (){

        console.log("borrando data")  
        this.queso={};
    }
}  
customElements.define('quesos-form', QuesoForm)    