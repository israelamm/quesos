import { LitElement, html, css } from 'lit-element';
import '../quesos-counter/quesos-counter'

class QuesosHeader extends LitElement {

    static get properties() {
        return {
        };
    }

    static get styles(){
        return css`
              div {
                  overflow:hidden;
                  background-color: #AAAAAA;
                  padding: 2px;
              }
        `
    }

    constructor() {
        super();
    }

    render() {
        return html`
        
            <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
            <div class="row">
                <div class="col-6">
                    <div>
                        <h5>App Quesos</h1>
                    </div>
                </div>
                <div class="col-6" align="rigth"> 
                    <quesos-counter></quesos-counter>
                </div>
            </div>
        `;
    }
}

customElements.define('quesos-header', QuesosHeader)