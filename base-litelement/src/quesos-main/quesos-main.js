import { LitElement, html } from 'lit-element';
import '../quesos-ficha-listado/quesos-ficha-listado';
import '../quesos-form/quesos-form';
import '../quesos-alert/quesos-alert';

class QuesosMain extends LitElement {
    
    static get properties() {
        return {
            quesos: {type: Array},
            showQuesoForm: {type: Boolean}
        };
    }

    constructor() {
        super();
        this.getQuesoData();
        this.showQuesoForm = false;
    }

    updated(changedProperties) { 
        console.log("updated");	
        if (changedProperties.has("showQuesoForm")) {
            console.log("Ha cambiado el valor de la propiedad showQuesoForm en quesos-main");
            if (this.showQuesoForm === true) {
                console.log("show en true")
                this.showQuesoFormData();
            } else {
                console.log("show en false")
                this.showQuesoList();
            }
        }

    }

    render() {
        return html`
            <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
            <h2 class="text-center">Quesos</h2>
            <div class="row" id="quesosList">
                <div class="row row-cols-1 row-cols-sm-4">
                ${this.quesos.map(
                    queso => html`<quesos-ficha-listado 
                                        name="${queso.name}" 
                                        description="${queso.description}"
                                        age="${queso.age}"
                                        .image="${queso.image}"
                                        recommended="${queso.recomended}"
                                        @delete-queso="${this.deleteQueso}"
                                        @info-queso="${this.infoQueso}"
                                    >
                                </quesos-ficha-listado>`
                )}
                </div>
            </div>
            <div class="row">
		        <quesos-form @quesos-form-store="${this.performStoreQueso}" class="border d-none rounded border-primary" id="quesoForm" @quesos-form-close="${this.quesoFormClose}">
		        </quesos-form>
	        </div>
            <quesos-alert class="d-none">>/quesos-alert>
        `;
    }

    infoQueso(e){
        console.log ("infoQueso en queso-main");
        console.log ("queso a mostrar: "+e.detail.name)
        this.shadowRoot.getElementById("quesoForm").queso = e.detail;
        this.showQuesoForm = true;


    }

    performStoreQueso(e){
        console.log("performFormStore");
        console.log("se va a agregar un queso");

        let indiceQueso = this.quesos.findIndex(queso=> queso.name === e.detail.queso.name);
        console.log("indice del queso: "+indiceQueso);

        if (indiceQueso>=0){
            this.quesos[indiceQueso]=e.detail.queso;
        }
        else{
            this.quesos.push(e.detail.queso);
        }

        console.log("queso almacenado");
        this.shadowRoot.getElementById("quesoForm").clearData();
        this.showQuesoForm=false;
        this.updateCounter();
        
    }

    deleteQueso(e) { 
        console.log("deleteQueso en quesos-main");
        console.log("Se va a borrar el queso de nombre " + e.detail.name);

        this.shadowRoot.querySelector("quesos-alert").classList.remove("d-none");

      
        this.quesos = this.quesos.filter(
            queso => queso.name != e.detail.name
        );
        this.updateCounter();
    }

    showQuesoList() {
        console.log("showQuesoList");
        console.log("Mostrando listado de quesos");
        this.shadowRoot.getElementById("quesosList").classList.remove("d-none");
        this.shadowRoot.getElementById("quesoForm").classList.add("d-none");	
        }  
    
    showQuesoFormData() {
        console.log("showQuesoFormData");
        console.log("Mostrando formulario de quesos");
        this.shadowRoot.getElementById("quesoForm").classList.remove("d-none");	  
        this.shadowRoot.getElementById("quesosList").classList.add("d-none");	
    }

    quesoFormClose() {
        console.log("quesoFormClose");
        console.log("Se ha cerrado el formulario de los quesos ");
        this.shadowRoot.getElementById("quesoForm").queso = {};
        this.showQuesoForm = false;
    }

    getQuesoData(){
        console.log ("en get quesos")
        let xhr = new XMLHttpRequest();
        xhr.onload = function (){
            if (xhr.status ===200){
                console.log("peticion correcta");
                //console.log("respuesta: "+xhr.responseText)
                let APIResponse = JSON.parse(xhr.responseText);
                //console.log("respuesta: ")
                this.quesos = APIResponse.cheeses ;
    
                console.log("al final"+this.quesos)
                this.updateCounter();
            }
        }.bind(this);
        xhr.open("GET","/data/quesos.json");
        xhr.send();
        
      }
    
      updateCounter(){
          console.log("cantidad de quesos: "+this.quesos.length);

          this.dispatchEvent(
              new CustomEvent('update-counter',{
                  detail:{
                      contador:this.quesos.length
                  }
              })
          );
          console.log("lanzo el evento0");
      }

}

customElements.define('quesos-main', QuesosMain)