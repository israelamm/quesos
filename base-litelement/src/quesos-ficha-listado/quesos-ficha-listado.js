import { LitElement, html } from 'lit-element';

class QuesosFichaListado extends LitElement {

    static get properties() {
        return {          
                name: {type:String},
                description: {type:String},
                age: {type:Number},
                image: {
                    src: {type:String},
                    height: {type:Number},
                    width: {type:Number}
                },
                recommended: true
        }
    }

    constructor() {
        super();
        this.name = "";
        this.description = "";
        this.age = 0;
        this.image = {};
        this.recommended = false; 
    }

    render() {
        return html`

        
            <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
            <div class="card h-100">
                <img src="${this.image.src}"  height="200" width="50" class="card-img-top"/>
                <div class="card-body">
                    <h5 class="card-title">${this.name}</h5>
                    <p class="card-text">${this.description}</p>
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item">${this.age} edad</li>
                    </ul>
                </div>
                <div class="card-footer">
			        <button @click="${this.deleteQueso}" class="btn btn-primary col-6">Borrar</button>
                    <button @click="${this.infoQueso}" class="btn btn-primary col-5">Info</button>
		        </div>	                
            </div>            
        `;
    }

    deleteQueso(e) {
        console.log("deleteQueso en quesos-ficha-listado");
        console.log("Se va a borrar el queso de nombre " + this.name); 
        
            this.dispatchEvent(
                new CustomEvent("delete-queso", {
                        detail: {
                            name: this.name
                        }
                    }
                )
            );
        }

    infoQueso(e){
        console.log("infoqueso en quesos-ficha-listado");
        console.log("Se va a mostrar la informacion del queso "+this.name)

        this.dispatchEvent(new CustomEvent('info-queso', {
                    detail:{
                        name: this.name,
                        description: this.description,
                        age: this.age,
                        image: this.image,
                        recommended: this.recommended
                    }
                }
            )
        );
    }
}

customElements.define('quesos-ficha-listado', QuesosFichaListado)