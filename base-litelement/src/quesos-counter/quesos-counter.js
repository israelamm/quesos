import { LitElement, html, css } from 'lit-element';

class QuesosCounter extends LitElement {

    static get properties() {
        return {
            cantidadQuesos : {type:Number}
        };
    }

    static get styles(){
        return css`
              div {
                  overflow:hidden;
                  background-color: #AAAAAA;
                  padding: 2px;
                  text-align:right;
              }
        `
    }

    constructor() {
        super();
        this.cantidadQuesos = 0;
    }

    render() {
        return html`
        
            <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
            <div> 
                <h5>Existen ${this.cantidadQuesos} quesos<h4>
            </div>
        `;
    }
}

customElements.define('quesos-counter', QuesosCounter)