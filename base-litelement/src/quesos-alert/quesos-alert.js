import { LitElement, html, css } from 'lit-element';

class QuesosAlert extends LitElement {

    static get properties() {
        return {
            cantidadQuesos : {type:Number}
        };
    }


    render() {
        return html`
        
            <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
            <div style="position: absolute; left: 0px; top: 40px; width:100%; align:center;">
                <div style="width:500px">
                <div class="card h-100">
                    <div class"card-header"><h4>Aviso</h4></div>
                    <div class="card-body">
                        <p class="card-text">Desea borrar el elemento?</p>
                    </div>
                    <div class="card-footer">
			            <button  @click="${this.deleteItem}" class="btn btn-primary col-6">Cancelar</button>
                        <button  class="btn btn-primary col-5">Borrar</button>
		            </div>	                
                </div>
                </div>
            </div>            
        `;
    }

    deleteItem(){
        this.classList.add("d-none")
    }
}

customElements.define('quesos-alert', QuesosAlert)