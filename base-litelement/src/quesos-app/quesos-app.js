import { LitElement, html } from 'lit-element';
import '../quesos-header/quesos-header.js';
import '../quesos-main/quesos-main.js';
import '../quesos-footer/quesos-footer';
import '../quesos-sidebar/quesos-sidebar';

class QuesosApp extends LitElement {

    static get properties() {
        return {
        };
    }

    constructor() {
        super();
    }

    render() {
        return html`
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
            <quesos-header></quesos-header>
            <div class="row">
				<quesos-sidebar @new-queso="${this.newQueso}" class="col-2" ></quesos-sidebar>
				<quesos-main class="col-10" @update-counter="${this.updateCounter}"></quesos-main>
			</div>	
            <quesos-footer></quesos-footer>
        `;
    }

    newQueso(e) {
        console.log("newQueso en quesosApp");	
        this.shadowRoot.querySelector("quesos-main").showQuesoForm = true;
    }

    updateCounter(e){
        console.log("en app updatecounter" +e.detail.contador);
        this.shadowRoot.querySelector("quesos-header").shadowRoot.querySelector("quesos-counter").cantidadQuesos = e.detail.contador;
    }
}

customElements.define('quesos-app', QuesosApp)